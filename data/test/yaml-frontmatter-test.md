---
title: 'YAML Frontmatter Test'
author: Me, Myself & I
date: 2020-11-24
end: 2020-11-27
price: 35.99
tags: [#yaml, #test, #2020]
---
# YAML Frontmatter Test
This is a test for the [[YAML Frontmatter]] Feature in Markdown.
## Subtopic
This is *some* __extra__  ~~markup~~ to irritate the [[Parser]].

And | a | table
--- | --- | ---
to | test | lines
starting | with | dashes

and ending
with
ellipsis
...
