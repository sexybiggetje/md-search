class MarkdownFile {
	private var contents:String;
	private var lines:Array<String>;
	private var frontmatter:Map<String, Any>;

	public function new( contents:String ) {
		this.contents = contents;
		this.lines = ~/(\r\n|\r)/g.replace(this.contents, "\n").split("\n");
		this.frontmatter = new Map<String, Any>();

		this.parseFrontmatter();
	}

	public function getContents():String {
		return this.contents;
	}

	public function getFrontmatter():Map<String, Any> {
		return this.frontmatter;
	}

	public function getContentsAsLines():Array<String> {
		return this.lines;
	}

	public function getContentsAsLinesWithoutFrontMatter():Array<String> {
		// Todo, implement filter.
		return this.lines;
	}

	private function parseFrontmatter():Void {
		var frontmatterOpen:Bool = false;
		var yamlString:Array<String> = new Array<String>();

		for ( line in this.lines ) {
			if ( StringTools.trim( line ) == "---" ) {
				if ( frontmatterOpen == false ) {
					frontmatterOpen = true;
				} else {
					frontmatterOpen = false;
					continue;
				}
			}

			if ( frontmatterOpen == true && StringTools.trim( line ) != "---") {
				yamlString.push( line );
			}
		}

		if ( yamlString.length > 0 ) {
			this.parseYamlFrontmatter( yamlString );
		}
	}

	// For now use this simple parsing as hx-yaml isn't haxe 4 compatible
	private function parseYamlFrontmatter(yaml:Array<String>):Void {
		var er = ~/([A-Z0-9-_]+)[:]{1}(.*)/i;
		for ( line in yaml ) {
			if ( er.match( line ) ) {
				// Should convert types here
				var match:String = StringTools.trim( er.matched(2) );

				if ( StringTools.startsWith( match, "'" ) || StringTools.startsWith( match, "\"" ) ) {
					if ( StringTools.endsWith( match, match.substring(0,1) ) ) {
						// Fix encapsulated strings
						this.frontmatter[ er.matched(1) ] = match.substring( 1, match.length - 1 );
					}
				} else if ( StringTools.startsWith( match, "[" ) && StringTools.endsWith( match, "]" ) ) {
					// Split arrays
					match = match.substring( 1, match.length - 1 );
					var arrayMatches:Array<String> = match.split( "," );
					var trimmedMatches:Array<String> = new Array<String>();

					for ( aMatch in arrayMatches) {
						trimmedMatches.push( StringTools.trim( aMatch ) );
					}

					this.frontmatter[ er.matched(1) ] = trimmedMatches;

				} else {
					// Pass remaining as original string
					this.frontmatter[ er.matched(1) ] = match;
				}
			}
		}
	}
}