import Arguments;
import Analyzer;
import FileWalker;
import hx.files.File;
import captain.Command;
import haxe.ds.Option;

class MdSearch {

	static private var instance:MdSearch;

	public var arguments:Arguments;
	public var version:String = "0.1";

	static function main():Void {
		MdSearch.instance = new MdSearch();
	}

	public function new() {

		var args = Sys.args();
		var arguments = new Arguments();
		var command = new Command( args );
		command.name = "mdq";

		command.arguments = [
			{
				name: "path",
				description: "The fileset you want to work with"
			}
		];

		command.options = [
			{
				name: "key",
				shortName: "k",
				boolean: false,
				description: "Uses key in frontmatter for search and expressions, instead of global text scope"
			},
			{
				name: "range",
				shortName: "r",
				boolean: false,
				description: "Look for a date range in key, use n conjunction with -k and provide format Y-m-d..Y-m-d"
			},
			{
				name: "expression",
				shortName: "e",
				boolean: false,
				description: "Evaluates expression on frontmatter data"
			},
			{
				name: "search",
				shortName: "s",
				boolean: false,
				description: "Search text content, or frontmatter when -k, for term"
			},
			{
				name: "caseinsensitive",
				shortName: "i",
				boolean: true,
				description: "Search text content, or frontmatter when -k, case insensitive"
			},
			{
				name: "pattern",
				shortName: "p",
				boolean: false,
				description: "Uses glob pattern for searching instead of the generic *.md"
			},
			{
				name: "id",
				shortName: "id", /* Captain lib cant work without a shortName yet*/
				boolean: false,
				description: "Only match file if frontmatter contains specified id"
			},
			{
				name: "open",
				shortName: "o",
				boolean: true,
				description: "Opens results in default editor"
			},
			{
				name: "help",
				shortName: "h",
				boolean: true,
				description: "Displays help"
			},
			{
				name: "version",
				shortName: "v",
				boolean: true,
				description: "Displays version string"
			},
			{
				name: "verbose",
				shortName: "V",
				boolean: true,
				description: "Prints more output"
			}
		];

		arguments.key = switch (command.getOption("key")) {
			case Some(value): value;
			case None: null;
		};

		arguments.range = switch (command.getOption("range")) {
			case Some(value): value;
			case None: null;
		};

		arguments.expression = switch (command.getOption("expression")) {
			case Some(value): value;
			case None: null;
		};

		arguments.searchTerm = switch (command.getOption("search")) {
			case Some(value): value;
			case None: null;
		};

		arguments.caseInsensitiveSearch = switch (command.getOption("caseinsensitive")) {
			case Some(value): true;
			case None: false;
		};

		/*arguments.filePattern = switch (command.getOption("pattern")) {
			case Some(value): value;
			case None: null;
		};*/

		arguments.id = switch (command.getOption("id")) {
			case Some(value): value;
			case None: null;
		};

		arguments.open = switch (command.getOption("open")) {
			case Some(value): true;
			case None: false;
		};

		arguments.help = switch (command.getOption("help")) {
			case Some(value): true;
			case None: false;
		};

		arguments.version = switch (command.getOption("version")) {
			case Some(value): true;
			case None: false;
		};

		arguments.verbose = switch (command.getOption("verbose")) {
			case Some(value): true;
			case None: false;
		};

		this.arguments = arguments;

		this.arguments.filePattern = switch ( command.getArgument("path") ) {
			case Some(value): value;
			case None: "*.md";
		};

		if ( args.length == 0 || arguments.version == true || arguments.help == true || arguments.invalidParam != null ) {
			Sys.println( "md-search v" + version );
		}

		if ( args.length == 0 || arguments.help == true ) {
			Sys.println( command.getInstructions() );
			Sys.exit( 0 );
		}

		var lukeFileWalker:FileWalker = new FileWalker();
		var list:Array<File> = lukeFileWalker.process( this.arguments.filePattern );

		var analyzer:Analyzer = new Analyzer( this.arguments );

		var openCommand:String = "";
		switch ( Sys.systemName().toLowerCase() ) {
			case "linux", "bsd":
				openCommand = "xdg-open";
			case "mac":
				openCommand = "open";
			case "windows":
				openCommand = "start";
			default:
		}

		for ( file in list ) {
			analyzer.analyze( file );

			if ( this.arguments.open == true && analyzer.hasMatches == true && openCommand != "" ) {
				Sys.command( openCommand, [ file.toString().substring( 2, file.toString().length ) ] );
			}
		}
	}

}
