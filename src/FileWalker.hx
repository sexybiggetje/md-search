import hx.files.*;

class FileWalker {

	public var path:String;

	public function new() {

	}

	public function process( myPath:String = "*.md" ):Array<File> {

		this.path = myPath;

		var d:Dir = Dir.of( "." );
		var list:Array<File> =  d.findFiles( myPath );

		return list;
	}

}