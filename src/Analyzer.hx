import Arguments;
import hx.files.File;
import MarkdownFile;

class Analyzer {

	public var arguments:Arguments;
	public var currentFile:File;
	public var hasMatches:Bool;

	public function new( arguments:Arguments ) {
		this.arguments = arguments;
	}

	public function analyze( myFile:File ):Void {
		this.currentFile = myFile;
		this.hasMatches = false; // TODO: implement searching

		if ( this.arguments.verbose ) {
			Sys.println( "-- " + myFile );
		}

		var contents:String = myFile.readAsString();
		var markdownFile:MarkdownFile = new MarkdownFile( contents );

		// Temp
		//Sys.println( markdownFile.getFrontmatter()["title"] );

		// Simple line based search for now, will replace with better later
		var lines:Array<String> = markdownFile.getContentsAsLines();
		var reg:EReg = new EReg( this.arguments.searchTerm, ( this.arguments.caseInsensitiveSearch ) ? "i" : "" );
		// TODO this regex search is still unsafe!
		for ( i in 0...lines.length ) {
			if ( reg.match( lines[i] ) ) {
				this.hasMatches = true;
				Sys.println( myFile + ":" + (i+1) );
			}
		}

		if ( this.arguments.verbose && !this.hasMatches ) {
			Sys.println( "No matches" );
		}
	}

}