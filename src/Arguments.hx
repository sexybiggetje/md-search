class Arguments {
	public var key:String;
	public var expression:String;
	public var range:String;
	public var id:String;

	public var help:Bool = false;
	public var version:Bool = false;
	public var open:Bool = false;
	public var verbose:Bool = false;
	public var invalidParam:String;
	public var invalidArgumentToParam:Bool;

	public var searchTerm:String;
	public var caseInsensitiveSearch:Bool;
	public var filePattern:String = "*.md";

	public function new() {}
}