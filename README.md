Makes markdown files searchable.

Fast implementation for Florian Schmidt according to this thread;

https://mstdn.social/@schmidt_fu/105259997773586571

Most options aren't implemented yet, this is a wishlist at the moment.

```bash
Usage: mdq <path> [options]

Arguments:
  path                   The fileset you want to work with

Options:
  --key, -k              (optional) Uses key in frontmatter for search and expressions, instead of global text scope
  --range, -r            (optional) Look for a date range in key, use n conjunction with -k and provide format Y-m-d..Y-m-d
  --expression, -e       (optional) Evaluates expression on frontmatter data
  --search, -s           (optional) Search text content, or frontmatter when -k, for term
  --caseinsensitive, -i  (optional) Search text content, or frontmatter when -k, case insensitive
  --pattern, -p          (optional) Uses glob pattern for searching instead of the generic *.md
  --id, -id               (optional) Only match file if frontmatter contains specified id
  --open, -o             (optional) Opens results in default editor
  --help, -h             (optional) Displays help
  --version, -v          (optional) Displays version string
  --verbose, -V          (optional) Prints more output
```

```bash
bin/mdq data/test/*.md -s "Feature" -V
```

```bash
sudo add-apt-repository ppa:haxe/releases -y
apt install haxe
haxelib setup
haxelib install build.hxml
haxe build.hxml
bin/mdq
```
